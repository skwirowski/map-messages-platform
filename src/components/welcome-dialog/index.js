import React from 'react';
import './style.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function WelcomeDialog(props) {
	return(
    <div>
      <Dialog
        open={true}
        onClose={props.onClick}
        aria-labelledby="welcome-dialog-title"
        aria-describedby="welcome-dialog-description"
      >
        <DialogTitle id="welcome-dialog-title">{"Welcome"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="welcome-dialog-description">
            Laboris velit laboris sit sit mollit mollit Lorem ad reprehenderit labore deserunt quis..
          </DialogContentText>
        </DialogContent>
        <DialogActions id="welcome-dialog-actions">
          <Button
            variant="contained"
            color="primary" autoFocus
            onClick={props.onClick}
          >
            Next
          </Button>
        </DialogActions>
      </Dialog>
    </div>
	);
}

/*class WelcomeDialog extends React.Component {
	render() {
		return (
			<div>
				<Dialog
					open={true}
					onClose={this.handleClose}
					aria-labelledby="welcome-dialog-title"
					aria-describedby="welcome-dialog-description"
				>
					<DialogTitle id="welcome-dialog-title">{"Welcome"}</DialogTitle>
					<DialogContent>
						<DialogContentText id="welcome-dialog-description">
							Laboris velit laboris sit sit mollit mollit Lorem ad reprehenderit labore deserunt quis..
            </DialogContentText>
					</DialogContent>
					<DialogActions id="welcome-dialog-actions">
						<Button
							variant="contained"
							color="primary" autoFocus
							onClick={this.handleClose}
						>
							Next
            </Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}*/

export default WelcomeDialog;