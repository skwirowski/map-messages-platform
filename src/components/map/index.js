import React, { PureComponent } from 'react';
import mapboxgl from 'mapbox-gl/dist/mapbox-gl';
import './style.css';

class Map extends PureComponent {
  componentDidMount() {
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFjaWVrbXAiLCJhIjoiY2ptcWUybzg0MDJidDNrbzF1MzBiMmQxbSJ9.-snbYDUJbdhu2i9z5DMQ0g';
    this.map = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
      center: [-74.50, 40], // starting position [lng, lat]
      zoom: 9 // starting zoom
    });
  }

  render() {
    return (
      <div id="map">
      </div>
    );
  }
}

export default Map;
