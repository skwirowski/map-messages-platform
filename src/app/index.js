import React, { PureComponent, Fragment } from 'react';
import Map from '../components/map/index';
import WelcomeDialog from '../components/welcome-dialog/index';
import LocationDialog from '../components/location-dialog';
import './style.css';

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isWelcomeClicked: false,
    }
  }

  handleWelcomeClick = () => this.setState({isWelcomeClicked: true});

  render() {
    const dialogToDisplay = () => {
      if(this.state.isWelcomeClicked) {
        return <LocationDialog />
      }
      return <WelcomeDialog onClick={this.handleWelcomeClick}/>
    };

    return (
      <Fragment>
        <Map />
        {dialogToDisplay()}
      </Fragment>
    );
  }
}

export default App;